#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

    ip r f all
    ip a f dev enp3s0
    systemctl stop NetworkManager
    ip a
    [..]
    2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc
    ip r
    [no sale nada]
```
systemctl status NetworkManager
   ● NetworkManager.service - Network Manager
   Loaded: loaded (/usr/lib/systemd/system/NetworkManager.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Tue 2016-11-15 12:53:28 CET; 2min 51s ago
     Docs: man:NetworkManager(8)
  Process: 675 ExecStart=/usr/sbin/NetworkManager --no-daemon (code=exited, status=0/SUCCESS)
 Main PID: 675 (code=exited, status=0/SUCCESS)
    Tasks: 1 (limit: 512)

```
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

```
ip a a 2.2.2.2/24 dev enp3s0
ip a a 3.3.3.3/16 dev enp3s0
ip a a 4.4.4.4/25 dev enp3s0

```

Consulta la tabla de rutas de tu equipo
```    
[root@j14 ~]# ip r
2.2.2.0/24 dev enp3s0  proto kernel  scope link  src 2.2.2.2 
3.3.0.0/16 dev enp3s0  proto kernel  scope link  src 3.3.3.3 
4.4.4.0/25 dev enp3s0  proto kernel  scope link  src 4.4.4.4 
```

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 contesta

2.2.2.254 host unreacheable, se queda esperando porque no encuentra un hosta con esa IP

2.2.5.2 network is unreacheable, no funciona porque la IP no esta incluida en la direccion de rec con la mascara

3.3.3.35 host unreacheable, se queda esperando porque no encuentra un hosta con esa IP

3.3.200.45 host unreacheable, se queda esperando porque no encuentra un hosta con esa IP

4.4.4.8 host unreacheable, se queda esperando porque no encuentra un hosta con esa IP

4.4.4.132 network is unreacheable, no funciona porque la IP no esta incluida en la direccion de rec con la mascara


#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet
    
    ip r f all
    ip a f dev enp3s0
    
Conecta una segunda interfaz de red por el puerto usb

Cambiale el nombre a usb0
```
ip link set usb0 name usb0
ip a
[..]
4: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

```
Modifica la dirección MAC

    ip link set dev usb0 address 00:11:22:33:44:13

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.
```
[root@j14 ~]# ip a a 5.5.5.5/24 dev usb0
[root@j14 ~]# ip a a 7.7.7.7/24 dev enp3s0


```

Observa la tabla de rutas
```
[root@j14 ~]# ip r
7.7.7.0/24 dev enp3s0  proto kernel  scope link  src 7.7.7.7 

```

#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

Lanzar iperf en modo servidor en cada ordenador

Comprueba con netstat en qué puerto escucha

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas
