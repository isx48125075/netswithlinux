practica hecha por:
Tito Arroyo j15
Roger Corral Calero j14
Miguel Rentero Ruiz j13

BRIDGES Y ROUTERS

##### 1. Conexionado, nombres de interfaces y direcciones MAC

Para esta práctica hay que interconectar los equipos de una fila con 2 latiguillos de red (sin usar las rosetas del aula) siguiendo el siguiente esquema:

```
FILA DE 3 EQUIPOS:
                              |usbB                          
   +--------+            +--------+            +--------+   
   |        |eth0    usbA|        |usbC    eth0|        |   
   | HOST A +------------+ HOST B +------------+ HOST C |   
   |        |            |        |            |        |   
   +--------+            +--------+            +--------+   
                             |eth0                          
```

El equipo del medio de la fila ha de tener conectadas 3 tarjetas usb-ethernet. Se han de renombrar estas tarjetas y cambiar las mac. Las direcciones mac de cada tarjeta han de ser distintas siguiendo la siguiente
codificación:

AA:AA:AA:00:00:YY

Siendo YY la numeración de la tarjeta USB que lleva en su pegatina.

Hay que renombrar las tarjetas de red para que se llamen: usbA, usbB, usbC y eth0(la de la placa base)

Al final la orden ip link show ha de mostrar algo como lo siguiente:

name_from_fedora_A=eth0
name_from_fedora_B=eth1
name_from_fedora_C=usb0

label_A=15
label_B=14
label_C=13

ip link set $name_from_fedora_A down
ip link set $name_from_fedora_B down
ip link set $name_from_fedora_C down

ip link set $name_from_fedora_A name usbA
ip link set $name_from_fedora_B name usbB
ip link set $name_from_fedora_C name usbC

ip link set usbA address aa:aa:aa:00:00:$label_A
ip link set usbB address aa:aa:aa:00:00:$label_B
ip link set usbC address aa:aa:aa:00:00:$label_C

ip link set usbA up
ip link set usbB up
ip link set usbC up


##### 2. Conectar 3 equipos haciendo routing y saliendo a internet:

A. Configurar las ips en cada equipo según el esquema y hacer pings desde hostA a hostB y desde hostC a hostA

```
ESQUEMA CAPA 3
                       +---------+
    172.17.15.0/24   .1| ROUTER  |.1   172.17.13.0/24
   +-------------------+ HOST B  +--------------------+
        |.2        usbA|         |usbC        |.2
        |eth0          +---------+            |eth0
     +------+                              +------+
     |HOST A|                              |HOST C|
     +------+                              +------+
                            
                            

```
host A:
ip a a 172.17.15.2/24 dev enp3s0
ping 172.17.13.1

host C:
ip a a 172.17.13.2/24 dev enp2s0
ping 172.17.15.1

host B:
ip a a 172.17.13.1/24 dev usbC
ip a a 172.17.15.1/24 dev usbA

B. Activar el bit de forwarding y poner como default gateway al host B que hará de router entre las dos redes, listar las rutas en host B y verificar que se pueden hacer pings entre HOST A y HOST C
echo 1 > /proc/sys/net/ipv4/ip_forward
host A:
ip r a default via 172.17.15.1
ping 172.17.13.2

host C:
ip r a default via 172.17.13.1
ping 172.17.15.2

host B:
```
[root@j14 isx48125075]# ip r
default via 192.168.0.5 dev enp3s0 
172.17.13.0/24 dev usbC  proto kernel  scope link  src 172.17.13.1 
172.17.15.0/24 dev usbA  proto kernel  scope link  src 172.17.15.1 
192.168.0.0/16 dev enp3s0  proto kernel  scope link  src 192.168.3.14 
```

C. Conectar eth0 del HOST B a la roseta del aula, pedir una ip dinámica con dhclient. Verificar que desde el HOST B se puede hacer ping al 8.8.8.8. 
host B:
dhclient enp3s0
ping 8.8.8.8 

D. Activar el enmascarmiento de ips para el tráfico saliente por eth0 y verificar que HOST A Y HOST C pueden hacer ping al 8.8.8.8
iptables -t nat -A POSTROUTING -o enp3s0 -j MASQUERADE

host A i C pueden hacer ping al 8.8.8.8

E. Poner como servidor dns a 8.8.8.8 en /etc/resolv.conf en hostA y host C y verificar que se puede navegar por internet con  haciendo un wget de http://www.netfilter.org
host A i C:
echo "nameserver 8.8.8.8" > /etc/resolv.conf

ahora host a i c pueden navegar por internet

##### 3. Conectar 2 equipos utilizando un equipo intermedio que hace de bridge. 

En esta práctica queremos que HOST B trabaje como si fuera un switch, interconectando a host A y host C. 

A. Hacer flush de todas las ips y verificar que no queda ninguna ruta ni dirección ip asociada a ningún equipo.
host A i C:
ip r f all
ip a f dev enp3s0

host B:
ip r f all
ip a f dev enp3s0
ip a f dev usbA
ip a f dev usbB
ip a f dev usbC
dhclient -r

B. Crear el bridge br0 y añadir usbA y usbC a ese bridge. Listar con detalle la configuración del bridge.
```
brctl addbr br0
brctl addif br0 usbA
brctl addif br0 usbC

[root@j14 scripts]# brctl show
bridge name	bridge id		STP enabled	interfaces
br0		8000.aaaaaa000013	no			usbA
										usbC

```


C. En el host A nos ponemos la ip 192.168.100.A/24 y en el equipo C la ip 192.168.100.C/24. Hacemos ping entre los dos equipos y debería de ir.
el ping funciona

D. Listar en host B la tabla de relación de puertos y MACs en el bridge

[root@j14 scripts]# brctl showmacs br0
port no	mac addr		is local?	ageing timer
  1	74:d0:2b:c9:f7:7b	no		  22.87
  2	94:de:80:89:45:b3	no		  22.87
  2	aa:aa:aa:00:00:13	yes		   0.00
  2	aa:aa:aa:00:00:13	yes		   0.00
  1	aa:aa:aa:00:00:15	yes		   0.00
  1	aa:aa:aa:00:00:15	yes		   0.00


##### 4. Conectar todos los equipos del aula haciendo switching.

Conseguir esta interconexión entre las filas:
```
\
                   |                     
                   +------+              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+              
 +------+ +------+        |              
                          |              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+
 +------+ +------+        |
                          |
                          +
```
Cada equipo ha de tener una ip 192.168.100.B/24

A. Lanzar fpings para verificar que todos los equipos A y C responden

B. Asignar una ip al br0 del hostB y lanzar fping para verificar que todos los equipos A,B y C responden

C. Listar la tabla de asignación de puertos y MACs después de hacer un fping 

##### 5. Conectar todos los equipos del aula haciendo routing.

```
\
                         +
                         |
                         |
                         |
                     +---+-----+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2            |         |          .2|
      |              +---------+            |
   +--+---+              |.1             +--+---+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |172.16.XX.0/24
                         |
                         |
                         |
                     usbB|.2
                     +---------+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2        usbA|         |usbC        |.2
      |eth0          +---------+            |eth0
   +------+          eth0|.1             +------+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |
                         +
```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden

##### 6. Conectar todos los equipos del aula haciendo switching y routing.

Seguir este esquema


```
                                       172.16.0.0/24
           +------------------------------------------------------------------------------+
                        |.XX                                                     |.XX
                        |                                                        |
                        |br0(usbB,eth0)                                          |br0(usbB,eth0)
                    +---------+                                              +---------+
 172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24        172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
+-------------------+ HOST B  +--------------------+     +-------------------+ HOST B  +--------------------+
     |.2        usbA|         |usbC        |.2                |.2        usbA|         |usbC        |.2
     |eth0          +---------+            |eth0              |eth0          +---------+            |eth0
  +------+                              +------+           +------+                              +------+
  |HOST A|                              |HOST C|           |HOST A|                              |HOST C|
  +------+                              +------+           +------+                              +------+

```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden
