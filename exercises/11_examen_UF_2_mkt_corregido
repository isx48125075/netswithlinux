Examen UF 2 - Routing sobre mikrotik - Marzo 2017 - Profesor: Alberto Larraz


Un bar desea dar wifi a sus clientes y quiere aprovechar la salida a 
internet que tiene contratada para conectar  un ordenador, los datáfonos y el
TPV, además desea conectar unas cámaras IP y unos televisores donde se 
emitirá contenido de internet controlado por una empresa externa

Dispondremos de 5 redes diferenciadas con las siguienes familias de IP:

Red A. 10.8.0.0/24     - Red interna para TPV, ordenador y datáfonos
Red B. 10.80.0.0/24    - WIFI de acceso libre
Red C. 10.180.0.0/24   - Cámaras IP
Red D. 10.180.1.0/24   - TV
Red E. 192.168.88.0/24 - configuración del router

El router del proveedor de servicios de internet se configura en modo
DMZ,  que deja pasar todo el tráfico entrante contra nuestro router interno.
Ip del router ISP: 192.168.200.101/24
Ip de nuestro router: 192.168.200.2XX/24

Configuración de puertos:
Puerto 4: salida a router ISP
Puerto 1: Red A
Puerto 2: Red E de configuración del router (192.168.88.1/24)
Wifi: Red B
Puerto 3: Usaremos interfaces virtuales VLANs:
  - vlan 10: Red C
  - vlan 20: Red D

Resolución por pasos que se ha de documentar para el examen:


1. Configurar IPs manuales en puertos 1,2 y 4. Verificar que desde 
el router se sale a internet (1 pt)

```
ip address add address=10.8.0.1/24 interface=eth1
ip address add address=192.168.200.218/24 interface=eth4 

#masquerade y default gateway
/ip firewall nat add action=masquerade chain=srcnat out-interface=eth4
/ip route add dst-address=0.0.0.0/0 gateway=192.168.200.101 

#ping 
/ping 8.8.8.8

```


2. Configurar servidores dhcp en redes A,B (1 pt)

```
/ip pool add name=rangoA ranges=10.8.0.10-10.8.0.50
/ip dhcp-server network add disabled=no dns-server=8.8.8.8 gateway=10.8.0.1 netmask=24 domain=prova.com address=10.8.0.0/24
/ip dhcp-server add address-pool=rangoA interface=eth1

/ip pool add name=rangoB ranges=10.80.0.10-10.80.0.50
/ip dhcp-server network add disabled=no dns-server=8.8.8.8 gateway=10.80.0.1 netmask=24 domain=prova.com address=10.80.0.0/24
/ip dhcp-server add address-pool=rangoB interface=wlan1

```


3. Habilitar acceso wifi libre (1 pt)

```
interface wireless set wlan1 disabled=no
interface wireless set ssid=mktXX wlan1 
```


4. Verificar que sale a internet haciendo NAT conectándose vía wifi o
cableada por redes A y B (1 pt). Llamar al profesor para que lo verifique


5. Configurar interfaces virtuales en el puerto 3 y darles IP (1 pt)

```
/interface vlan add name vlan10 vlan-id=10 interface=eth3
/ip address add address=10.180.0.1/24 interface=vlan10 

/interface vlan add name vlan20 vlan-id=20 interface=eth3 
/ip address add address=10.180.1.1/24 interface=vlan20 
```


6. Verificar que sale a internet conectando el PC con una interfaz virtual
de la vlan 10 con una ip manual (1pt)

```
#En el linux del PC:
ip link add link enp2s0 name vlan10 type vlan id 10
ip a a 10.180.0.2/24 dev vlan10

```


7. Crear un nuevo perfil de seguridad en la red wifi que pida contraseña
WPA2 "graciasporlawifi" y aplicarla a la wifi actual (1pt)

```
#Añadimos el nuevo perfil
interface wireless security-profile add name=newprofile wpa2-pre-shared-key \
authentication-types=wpa2-psk mode=dynamic-keys

#Asignamos el nuevo perfil
interface wireless set security-profile=newprofile 0

```


8. Crear reglas de firewall que limiten el acceso desde las redes C y D 
a la red wifi (1pt)

```
/ip firewall 
add chain=forward dst-address=10.80.0.0/24 src-address=10.180.0.0/23 action=drop 
add chain=forward src-address=10.80.0.0/24 dst-address=10.180.0.0/23 action=drop 
```

9. Segurizar que sólo se pueda entrar por ssh al router por el puerto 28022
desde la interfaz 4 (0,5pt) y que el resto de tráfico entrante 
esté bloqueado en ese puerto(0,5pt)

```
/ip service set [find name=ssh] port=28022
/ip firewall 
filter add chain=input protocol=tcp dst-port=2822 in-interface=eth4 action=accept
filter add chain=input protocol=tcp in-interface=eth4 action=drop

```

10. Configurar la mikrotik con el nombre "mktInterna", sincronizar el 
reloj interno, hacer un backup y redirigir el puerto 9080 al puerto 80 
de la ip de la cámara de seguridad 10.180.0.101 (1 pt)

```
/system identity set name=mktInterna
/system ntp client
set enabled=yes primary-ntp=213.251.52.234 secondary-ntp=158.227.98.15

/backup save name="20170420_conf"
/ip firewall nat
add action=dst-nat chain=dstnat disabled=no dst-port=9080 \
in-interface=eth1 protocol=tcp to-addresses=10.180.0.101 to-ports=80 
```
