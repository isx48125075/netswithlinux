#EJEMPLO PARA EL PUESTO21

/interface vlan add vlan-id=721 interface=ether3 name=deptA
/interface vlan add vlan-id=821 interface=ether3 name=deptB
/interface vlan add vlan-id=921 interface=ether3 name=servers
/interface vlan add vlan-id=1000 interface=ether1 name=escola
/interface vlan add vlan-id=1005 interface=ether1 name=isp

/ip address add interface=deptA address=10.27.121.1/24
/ip address add interface=deptB address=10.28.121.1/24
/ip address add interface=servers address=10.29.121.1/24
/ip address add interface=escola address=192.168.3.221/16
/ip address add interface=isp address=10.30.0.121/24

#

interface FastEthernet0/4
 switchport trunk allowed vlan 1000,1005
 switchport mode trunk
!
interface FastEthernet0/5
 switchport access vlan 1000
 switchport trunk allowed vlan 721,821,921
 switchport mode trunk

interface FastEthernet0/17
 switchport trunk allowed vlan 719,819,919
 switchport mode trunk
!
interface FastEthernet0/16
 switchport trunk allowed vlan 1000,1005
 switchport mode trunk
