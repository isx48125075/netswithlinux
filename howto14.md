#1.
quitamos el networkmanager y apagamos el dhclient
```
systemctl stop NetworkManager
```
dhclient -r

pongo las ips en la tarjeta de red del host
```
[root@j14 isx48125075]# ip a a 10.8.8.114/16 dev enp3s0

[root@j14 isx48125075]# ip a a 10.7.14.100/24 dev enp3s0
```


el ping funciona
```
[root@j14 isx48125075]# ping 10.8.1.1
PING 10.8.1.1 (10.8.1.1) 56(84) bytes of data.
64 bytes from 10.8.1.1: icmp_seq=1 ttl=64 time=0.320 ms
64 bytes from 10.8.1.1: icmp_seq=2 ttl=64 time=0.214 ms
64 bytes from 10.8.1.1: icmp_seq=3 ttl=64 time=0.218 ms
64 bytes from 10.8.1.1: icmp_seq=4 ttl=64 time=0.173 ms
64 bytes from 10.8.1.1: icmp_seq=5 ttl=64 time=0.188 ms
64 bytes from 10.8.1.1: icmp_seq=6 ttl=64 time=0.166 ms
```
el ping funciona

```
[root@j14 isx48125075]# ping 10.7.14.1
PING 10.7.14.1 (10.7.14.1) 56(84) bytes of data.
64 bytes from 10.7.14.1: icmp_seq=1 ttl=64 time=0.329 ms
64 bytes from 10.7.14.1: icmp_seq=2 ttl=64 time=0.205 ms
64 bytes from 10.7.14.1: icmp_seq=3 ttl=64 time=0.186 ms
64 bytes from 10.7.14.1: icmp_seq=4 ttl=64 time=0.210 ms
64 bytes from 10.7.14.1: icmp_seq=5 ttl=64 time=0.197 ms
```

#2.
añado otra ip a la tarjeta de red del host
```
[root@j14 isx48125075]# ip a a 10.9.9.214/24 dev enp3s0
```
añado una ruta estatica

```
[root@j14 isx48125075]# ip r a 10.6.6.0/24 via 10.9.9.1
```
el ping funciona
```
[root@j14 isx48125075]# ping 10.6.6.2
64 bytes from 10.6.6.2: icmp_seq=1 ttl=63 time=0.836 ms
64 bytes from 10.6.6.2: icmp_seq=2 ttl=63 time=0.575 ms
64 bytes from 10.6.6.2: icmp_seq=3 ttl=63 time=0.691 ms
64 bytes from 10.6.6.2: icmp_seq=4 ttl=63 time=0.719 ms
```
#3.
el ping funciona

```
[root@j14 isx48125075]# ping 10.6.6.2
```
guardo la captura de paquetes en /tmp/ping14.pcap usando tshark

```
[root@j14 isx48125075]# tshark -i enp3s0 -c 2 -w /tmp/ping14 icmp
Running as user "root" and group "root". This could be dangerous.
Capturing on 'enp3s0'
2
```
observamos el ttl por pantalla 
```
[root@j14 isx48125075]# tshark -r /tmp/ping14.pcap 
Running as user "root" and group "root". This could be dangerous.
  1 0.000000000   10.9.9.214 → 10.6.6.2     ICMP 98 Echo (ping) request  id=0x0d41, seq=13/3328, ttl=64
  2 0.000563504     10.6.6.2 → 10.9.9.214   ICMP 98 Echo (ping) reply    id=0x0d41, seq=13/3328, ttl=63 (request in 1)
```

#4.
canvio el nombre del usb
```
[root@j14 isx48125075]# ip link set usb0 name usb14
```
le canvio la mac
```
[root@j14 isx48125075]# ip link set usb14 address 44:44:44:00:00:14
```
subo la interficie
```
[root@j14 isx48125075]# ip link set usb14 up
```
```
4: usb14: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 44:44:44:00:00:14 brd ff:ff:ff:ff:ff:ff
    inet 10.5.5.114/24 scope global usb14
       valid_lft forever preferred_lft forever
    inet6 fe80::4644:44ff:fe00:14/64 scope link 
       valid_lft forever preferred_lft forever
```
#5.
quito todas las ip del usb14
```
[root@j14 isx48125075]# ip a f dev usb14
```
añado una ip a la interficie usb14
```
[root@j14 isx48125075]# ip a a 172.17.0.1/16 dev usb14
```
pongo en 1 el bit de forwarding
```
[root@j14 isx48125075]# echo 1 > /proc/sys/net/ipv4/ip_forward
```
#6.
ejecutamos dhclient para volver a tener conexion a internet
```
[root@j14 isx48125075]# dhclient 
```
guardo los saltos hasta papua.go.id en /tmp/papua14.txt
```
[root@j14 isx48125075]# traceroute papua.go.id > /tmp/papua14.txt
```
#7.
busco en que puertos escucha el servidor cups con netstat
```
[root@j14 isx48125075]# netstat -utlnp | grep cups
tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      1426/cupsd          
tcp6       0      0 ::1:631                 :::*                    LISTEN      1426/cupsd 
```




